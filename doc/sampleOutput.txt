[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 10000

QuickSort: Sorted.
Elapsed time: 0.00127254

Randomized QuickSort: Sorted.
Elapsed time: 0.00129488

MergeSort: Sorted.
Elapsed time: 0.00167088

HeapSort: Sorted.
Elapsed time: 0.00308317

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 60000

QuickSort: Sorted.
Elapsed time: 0.00936542

Randomized QuickSort: Sorted.
Elapsed time: 0.00936314

MergeSort: Sorted.
Elapsed time: 0.0110109

HeapSort: Sorted.
Elapsed time: 0.0238317

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 110000

QuickSort: Sorted.
Elapsed time: 0.0182506

Randomized QuickSort: Sorted.
Elapsed time: 0.0194418

MergeSort: Sorted.
Elapsed time: 0.0218546

HeapSort: Sorted.
Elapsed time: 0.0455078

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 160000

QuickSort: Sorted.
Elapsed time: 0.0280872

Randomized QuickSort: Sorted.
Elapsed time: 0.0281713

MergeSort: Sorted.
Elapsed time: 0.0332069

HeapSort: Sorted.
Elapsed time: 0.0697199

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 210000

QuickSort: Sorted.
Elapsed time: 0.0380615

Randomized QuickSort: Sorted.
Elapsed time: 0.0380987

MergeSort: Sorted.
Elapsed time: 0.0433469

HeapSort: Sorted.
Elapsed time: 0.0926556

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 260000

QuickSort: Sorted.
Elapsed time: 0.0459131

Randomized QuickSort: Sorted.
Elapsed time: 0.0472436

MergeSort: Sorted.
Elapsed time: 0.0624557

HeapSort: Sorted.
Elapsed time: 0.149662

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 310000

QuickSort: Sorted.
Elapsed time: 0.0563237

Randomized QuickSort: Sorted.
Elapsed time: 0.0564848

MergeSort: Sorted.
Elapsed time: 0.0652074

HeapSort: Sorted.
Elapsed time: 0.142333

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 360000

QuickSort: Sorted.
Elapsed time: 0.0647406

Randomized QuickSort: Sorted.
Elapsed time: 0.0663881

MergeSort: Sorted.
Elapsed time: 0.0765739

HeapSort: Sorted.
Elapsed time: 0.166331

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 410000

QuickSort: Sorted.
Elapsed time: 0.076613

Randomized QuickSort: Sorted.
Elapsed time: 0.0771927

MergeSort: Sorted.
Elapsed time: 0.0871063

HeapSort: Sorted.
Elapsed time: 0.191062

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 460000

QuickSort: Sorted.
Elapsed time: 0.0849332

Randomized QuickSort: Sorted.
Elapsed time: 0.0852317

MergeSort: Sorted.
Elapsed time: 0.0974621

HeapSort: Sorted.
Elapsed time: 0.215108

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 510000

QuickSort: Sorted.
Elapsed time: 0.0931083

Randomized QuickSort: Sorted.
Elapsed time: 0.0925065

MergeSort: Sorted.
Elapsed time: 0.109568

HeapSort: Sorted.
Elapsed time: 0.23997

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 560000

QuickSort: Sorted.
Elapsed time: 0.105934

Randomized QuickSort: Sorted.
Elapsed time: 0.104341

MergeSort: Sorted.
Elapsed time: 0.121234

HeapSort: Sorted.
Elapsed time: 0.266596

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 610000

QuickSort: Sorted.
Elapsed time: 0.112872

Randomized QuickSort: Sorted.
Elapsed time: 0.111938

MergeSort: Sorted.
Elapsed time: 0.132228

HeapSort: Sorted.
Elapsed time: 0.291749

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 660000

QuickSort: Sorted.
Elapsed time: 0.124926

Randomized QuickSort: Sorted.
Elapsed time: 0.124756

MergeSort: Sorted.
Elapsed time: 0.142921

HeapSort: Sorted.
Elapsed time: 0.317559

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 710000

QuickSort: Sorted.
Elapsed time: 0.133303

Randomized QuickSort: Sorted.
Elapsed time: 0.132836

MergeSort: Sorted.
Elapsed time: 0.155358

HeapSort: Sorted.
Elapsed time: 0.343836

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 760000

QuickSort: Sorted.
Elapsed time: 0.14367

Randomized QuickSort: Sorted.
Elapsed time: 0.143397

MergeSort: Sorted.
Elapsed time: 0.166364

HeapSort: Sorted.
Elapsed time: 0.371169

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 810000

QuickSort: Sorted.
Elapsed time: 0.16079

Randomized QuickSort: Sorted.
Elapsed time: 0.15547

MergeSort: Sorted.
Elapsed time: 0.177638

HeapSort: Sorted.
Elapsed time: 0.397682

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 860000

QuickSort: Sorted.
Elapsed time: 0.162336

Randomized QuickSort: Sorted.
Elapsed time: 0.16266

MergeSort: Sorted.
Elapsed time: 0.188185

HeapSort: Sorted.
Elapsed time: 0.422397

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 910000

QuickSort: Sorted.
Elapsed time: 0.17566

Randomized QuickSort: Sorted.
Elapsed time: 0.176093

MergeSort: Sorted.
Elapsed time: 0.200573

HeapSort: Sorted.
Elapsed time: 0.452383

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 960000

QuickSort: Sorted.
Elapsed time: 0.18435

Randomized QuickSort: Sorted.
Elapsed time: 0.184087

MergeSort: Sorted.
Elapsed time: 0.21301

HeapSort: Sorted.
Elapsed time: 0.477642

[billy@wromansky lab6]$ ./bin/sort 
Enter the size of the arrays you want to sort: 1000000

QuickSort: Sorted.
Elapsed time: 0.188988

Randomized QuickSort: Sorted.
Elapsed time: 0.188723

MergeSort: Sorted.
Elapsed time: 0.221622

HeapSort: Sorted.
Elapsed time: 0.500118


------------------------------------------------------------------
QuickSort
0.00134377

0.00883357

0.0190659

0.0308898

0.0383826

0.0482413

0.0549227

0.0695452

0.0748044

0.0854305

0.099233

0.117556

0.119614

0.127031

0.136209

0.153469

0.159033

0.171368

0.172027

0.193456

0.195101

0.230288

------------------------------
Randomized QuickSort
0.00127547

0.00977103

0.0186266

0.0273532

0.0373713

0.0480849

0.0533674

0.0656453

0.0777546

0.0865285

0.0934721

0.114479

0.126631

0.129146

0.133714

0.156656

0.160037

0.170846

0.180516

0.182077

0.196927

0.228104

------------------------------
MergeSort
0.00155286

0.0103067

0.0213015

0.0323509

0.0436345

0.0515472

0.0623135

0.071208

0.0811972

0.091604

0.105632

0.114989

0.123827

0.134018

0.148218

0.154578

0.166032

0.179136

0.188699

0.202023

0.209149

0.219806

------------------------------
HeapSort
0.00312644

0.0223856

0.0475425

0.0677276

0.0891516

0.111428

0.132672

0.160425

0.178342

0.207351

0.231341

0.255141

0.273994

0.302334

0.325052

0.354178

0.380418

0.403261

0.426186

0.452591

0.474404

0.50574



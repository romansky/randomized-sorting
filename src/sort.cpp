#include "sort.h"

void isSorted(int* arr, int n)
{
    bool b = true;
    for(int i=0; i < n; i++)
    {
        if(arr[i] > arr[i+1])
        {
            n = false;
        }
    }
    if(b)
    {
        cout << "Sorted." << endl;
    }
}

void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void printArray(int* arr, int n)
{
    for(int i=0; i < n; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int* copyArray(int* arr, int n)
{
    int* a = new int[n];
    for(int i=0; i < n; i++)
    {
        a[i] = arr[i];
    }

    return a;
}

void shuffle(int* arr, int n)
{
    srand(time(0));
    for(int i=0; i < n; i++)
    {
        int j = rand() & i;
        swap(&arr[i],&arr[j]);
    }

}

int* makeArray(int n)
{
    int* a = new int[n];
    for(int i=0; i < n; i++)
    {
        a[i] = i+1;
    }

    return a;
} 

int partition(int* arr, int low, int high)
{
    int pivot = arr[high];
    int i = (low - 1);
    for(int j=low; j <= (high - 1); j++)
    {
        if(arr[j] <= pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i+1], &arr[high]);
    return (i+1);
}

void quickSort(int* arr, int low, int high)
{
    if(low < high)
    {
        int p = partition(arr, low, high);
        quickSort(arr,low,p-1);
        quickSort(arr,p+1,high);
    }
}

void randQS(int* arr, int low, int high)
{
    srand(time(0));
    if(low < high)
    {
        int p = low + rand() % (high-low);
        swap(&arr[p],&arr[low]);
        int index = partition(arr,low,high);
        quickSort(arr,low,index-1);
        quickSort(arr,index+1,high);
    }
}

void merge(int* arr, int l, int m, int r)
{
    int i,j,k;
    int n1 = m - l;
    int n2 = r - m + 1;

    int L[n1];
    int R[n2];

    for(i=0; i < n1; i++)
        L[i] = arr[l+i];

    for(j=0; j < n2; j++)
        R[j] = arr[m+j];

    i=0;
    j=0;
    k=l;

    while((i < n1) && (j < n2))
    {
        if(L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        } else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while(i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    while(j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int* arr, int l, int r)
{
    if(l < r)
    {
        int m = (l+r)/2;

        mergeSort(arr,l,m);
        mergeSort(arr,m+1,r);
        merge(arr,l,m+1,r);
    }
}

void heapify(int* arr, int n, int i)
{
    int max = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if ((l < n) && (arr[l] > arr[max]))
    {
        max = l;
    }

    if ((r < n) && (arr[r] > arr[max]))
    {
        max = r;
    }

    if (max != i)
    {
        swap(arr[i],arr[max]);
        heapify(arr,n,max);
    }
}

void heapSort(int* arr, int n)
{
    for(int i=n/2-1; i >= 0; i--)
    {
        heapify(arr,n,i);
    }

    for(int i=n-1; i>=0; i--)
    {
        swap(arr[0], arr[i]);
        heapify(arr,i,0);
    }
}

#ifndef SORT_H
#define SORT_H

#include <time.h>
#include <iostream>

using namespace std;

void isSorted(int* arr, int n);
void swap(int *a, int *b);
void printArray(int* arr, int n);
int* copyArray(int* arr, int n);
void shuffle(int* arr, int n);
int* makeArray(int n);
int partition(int* arr, int l, int r);
int randPart(int* arr, int l, int r);
void quickSort(int* arr, int l, int r);
void randQS(int* arr);
void merge(int* arr, int l, int m, int r);
void mergeSort(int* arr, int l, int r);
void heapify(int* arr, int n, int i);
void heapSort(int* arr, int n);

#include "sort.cpp"
#endif

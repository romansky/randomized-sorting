# Randomized Sorting

This program will experiment with randomized versions of standard sorting algorithms and statistically gather and interpret data.

## a
I first designed the main to accept user input and to test all the sorts on the user-specified array but found this to be too cumbersome to gather data this way. I commented out my old testing code and implemented for-loops that create arrays of size n=10,000 to 1,060,000 to be sorted sequentially.

## b
The best case of QuickSort is OMEGA(nlog(n)), the average THETA(nlog(n)), and the worst case as THETA(n^2).

The best case of MergeSort is OMEGA(nlog(n)), the average THETA(nlog(n)), and the worst case as O(nlog(n)).\

The best case of HeapSort is OMEGA(nlog(n)), the average THETA(nlog(n)), and the worst case as O(nlog(n)).

## c
As the array length increased, the time to sort them increased in such a way that matches the average case of THETA(nlog(n)). Something worth noting is that MergeSort began to segmentation fault at size 10,000,000 arrays. This is because int is capped at less than 10,000,000.

## d

Average Times:
QuickSort: 0.104811125
Randomized QuickSort: 0.104471973
MergeSort: 0.109687425
HeapSort: 0.245672352

## e
See the included Excel spreadsheets in docs/

## f
The shuffle routine is somewhat basic. It simply accepts an (assumed to be) already sorted array and randomly picks which indeces to swap to "randomly" shuffle the array. I saw there was a standard library method for the same purpose but decided not to use it. I believe there may be a difference in using srand(time(0)) and srand(time(null)) but I have not explored this possibility.

## g
To improve this code, I definitely want to be able fit all of the chronometer code into a function that passes a function pointer as a parameter. I also would want to find a way to make my shuffle method more efficient.

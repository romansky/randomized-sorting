#include "sort.h"
#include <iostream>
#include <time.h>
#include <chrono>

using namespace std;

int main()
{
    
    cout << "QuickSort" << endl;

    for(int size=10000; size < 1100000; size += 50000)
    {
        int* quickS = makeArray(size);
        shuffle(quickS,size);

        auto start = std::chrono::system_clock::now();
        quickSort(quickS,0,size-1);
        auto end = std::chrono::system_clock::now();

        //isSorted(qS,size);
        //printArray(qS,sz);

        std::chrono::duration<double> elapsed_seconds = end-start;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);
        cout << elapsed_seconds.count() << endl<< endl;

	delete [] quickS;
    }

    cout << "------------------------------" << endl;
    cout << "Randomized QuickSort" << endl;

    for(int size=10000; size < 1100000; size += 50000)
    {
        int* RquickS = makeArray(size);
        shuffle(RquickS,size);

        auto start = std::chrono::system_clock::now();
        randQS(RquickS,0,size-1);
        auto end = std::chrono::system_clock::now();

        //isSorted(qS,size);
        //printArray(qS,sz);

        std::chrono::duration<double> elapsed_seconds = end-start;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);
        cout << elapsed_seconds.count() << endl<< endl;

	delete [] RquickS;
    }

    
    cout << "------------------------------" << endl;
    cout << "MergeSort" << endl;

    for(int size=10000; size < 1100000; size += 50000)
    {
        int* mergeS = makeArray(size);
        shuffle(mergeS,size);

        auto start = std::chrono::system_clock::now();
        mergeSort(mergeS,0,size-1);
        auto end = std::chrono::system_clock::now();

        //isSorted(qS,size);
        //printArray(qS,sz);

        std::chrono::duration<double> elapsed_seconds = end-start;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);
        cout << elapsed_seconds.count() << endl<< endl;

	delete [] mergeS;
    }

    cout << "------------------------------" << endl;
    cout << "HeapSort" << endl;

    for(int size=10000; size < 1100000; size += 50000)
    {
        int* heapS = makeArray(size);
        shuffle(heapS,size);

        auto start = std::chrono::system_clock::now();
        heapSort(heapS,size);
        auto end = std::chrono::system_clock::now();

        //isSorted(qS,size);
        //printArray(qS,sz);

        std::chrono::duration<double> elapsed_seconds = end-start;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);
        cout << elapsed_seconds.count() << endl<< endl;

	delete [] heapS;
    }



    return 0;
}
